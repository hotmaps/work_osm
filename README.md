# work_osm



## Repository structure
```
data                    -- containts the dataset in Geotiff format
readme.md               -- Readme file
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```


## Documentation

This dataset shows densities of a selection of workplaces in Europe from Open Street Map [1].
Included countires are :
```
country_codes = ['AT', 'BE', 'BG', 'HR', 'CY', 'CZ', 'DK', 'EE', 'FI', 'FR', 'DE', 'GR', 'HU', 'IE', 'IT','LV', 'LT', 'LU', 'MT', 'NL', 'PL', 'PT', 'RO', 'SK', 'SI', 'ES', 'SE', 'AL', 'AD', 'AM', 'BY', 'BA', 'FO', 'GE', 'GI', 'IS', 'IM', 'XK', 'LI', 'MK', 'MD', 'MC', 'ME', 'NO', 'SM', 'RS', 'CH', 'TR', 'UA', 'GB', 'VA']

```

The requests of Points of Interests have been performed with the Overpass API [2] (free of charge).

The codes included in each density are listed below :
```
'work'= ['"building"="industrial"','"building"="office"','"company"','"landuse"="industrial"','"industrial"', '"office"', '"amenity"="research_institute"', '"amenity"="conference_centre"', '"amenity"="bank"', '"amenity"="hospital"','"amenity"="townhall"','"amenity"="police"','"amenity"="fire_station"', '"amenity"="post_office"', '"amenity"="post_depot"', '"office"="company"','"office"="government"',] ;
```

The pixel values are the sum of the number of results located in the pixel.

### Limitations of the dataset
- The dataset provides densities of only a selection of points of interests. The complete list of amenity codes can be fund on the [OSM Wiki](https://wiki.openstreetmap.org/wiki/Key:amenity#Sustenance).
- Ways are only considered throught their center points.


### References
[1] [Open Street Map](https://www.openstreetmap.org/)
[2] [Overpass API](http://overpass-api.de/api/interpreter)
[3] [OSM Wiki](https://wiki.openstreetmap.org/wiki/Key:amenity#Sustenance)



## How to cite
Jeannin Noémie, OPENGIS4ET Project WP3


## Authors
Jeannin Noémie <sup>*</sup>

<sup>*</sup> [EPFL, PV-Lab](https://www.epfl.ch/labs/pvlab/), Laboratory of photovoltaics and thin-films electronics, Rue de la Maladière 71b, CH-2002 Neuchâtel 2


## License
Copyright © 2023: Noémie Jeannin

Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## Acknowledgment
We would like to convey our deepest appreciation to the OPENGIS4ET Project, which provided the funding to carry out the present investigation.